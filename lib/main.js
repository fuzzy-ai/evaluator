// main.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const EvaluatorServer = require('./evaluator-server')

// XXX: send a message to Slack for uncaught exceptions

process.on('uncaughtException', (err) => {
  console.log('******************** UNCAUGHT EXCEPTION ********************')
  console.error(err)
  if (err.stack) {
    console.dir(err.stack.split('\n'))
  }
  return process.exit(127)
})

// No errors on too many listeners

process.setMaxListeners(0)

const server = new EvaluatorServer(process.env)

server.start((err) => {
  if (err) {
    return console.error(err)
  } else {
    return console.log('Server listening')
  }
})

// Try to send a slack message on error

process.on('uncaughtException', (err) => {
  if ((server != null) && (server.slackMessage != null)) {
    const msg = `UNCAUGHT EXCEPTION: ${err.message}`
    return server.slackMessage('error', msg, ':bomb:', (err) => {
      if (err != null) {
        return console.error(err)
      }
    })
  }
})

// Gracefully shutdown if we can

const shutdown = function () {
  console.log('Shutting down...')
  return server.stop((err) => {
    if (err) {
      console.error(err)
      return process.exit(-1)
    } else {
      console.log('Done.')
      return process.exit(0)
    }
  })
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)
