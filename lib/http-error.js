// http-error.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

class HTTPError extends Error {
  constructor (message, statusCode) {
    super()
    this.message = message
    this.statusCode = statusCode
  }
}

module.exports = HTTPError
