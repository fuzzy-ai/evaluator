// evaluator-server.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const Microservice = require('@fuzzy-ai/microservice')

const version = require('./version')
const RFC = require('@fuzzy-ai/fuzzy-controller')

class EvaluatorServer extends Microservice {
  getName () {
    return 'evaluator'
  }

  setupRoutes (exp) {
    exp.post('/evaluate', this.evaluate)
    exp.get('/version', this.getAppVersion.bind(this))
    exp.get('/live', this.dontLog, this.live)
    exp.get('/ready', this.dontLog, this.ready)

    return exp
  }

  live (req, res, next) {
    return res.json({status: 'OK'})
  }

  ready (req, res, next) {
    return res.json({status: 'OK'})
  }

  evaluate (req, res, next) {
    const { properties } = req.body
    const { inputs } = req.body

    let results = {
      fuzzified: null,
      inferred: null,
      clipped: {},
      combined: {},
      centroid: {},
      rules: []
    }

    try {
      const rfc = new RFC(properties)

      rfc.once('fuzzified', (fuzzified) => {
        results.fuzzified = fuzzified
      })
      rfc.on('rule', (rule) => {
        results.rules.push(rule)
      })
      rfc.once('inferred', (output) => {
        results.inferred = output
      })
      rfc.on('clipped', (clipped, name) => {
        results.clipped[name] = clipped
      })
      rfc.on('combined', (combined, name) => {
        results.combined[name] = combined
      })
      rfc.on('centroid', (centroid, name) => {
        results.centroid[name] = centroid
      })

      results.crisp = rfc.evaluate(inputs)

      res.json(results)

      results = null
    } catch (err) {
      return next(err)
    }
  }

  getAppVersion (req, res, next) {
    return res.json({
      name: this.getName(),
      version
    })
  }

  // This server doesn't connect to the database

  startDatabase (callback) {
    return callback(null)
  }

  stopDatabase (callback) {
    return callback(null)
  }
}

module.exports = EvaluatorServer
