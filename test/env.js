// env.js
// Copyright 2014,2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

module.exports = {
  HOSTNAME: 'localhost',
  PORT: '2342',
  LOG_FILE: '/dev/null',
  SILENT: 'true'
}
