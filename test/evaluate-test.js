// evaluator-server-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const apiBatch = require('./api-batch')
const lr = require('./lr')

lr()

vows
  .describe('Evaluate endpoint')
  .addBatch(apiBatch({
    'and we evaluate': {
      topic () {
        const { callback } = this
        const url = 'http://localhost:2342/evaluate'
        const properties = {
          inputs: {
            input1: {
              low: [0, 1],
              medium: [0, 1, 2],
              high: [1, 2]
            }
          },
          outputs: {
            output1: {
              low: [0, 1],
              medium: [0, 1, 2],
              high: [1, 2]
            }
          },
          rules: ['input1 INCREASES output1']
        }
        const inputs =
          {input1: 1.3}
        const options = {
          url,
          json: {
            properties,
            inputs
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, el) {
        assert.ifError(err)
        assert.isObject(el)
        assert.isObject(el.fuzzified)
        assert.isArray(el.rules)
        for (let i = 0; i < el.rules.length; i++) {
          const rule = el.rules[i]
          assert.isNumber(rule, `Rule ${i} is not a number: ${rule}`)
        }
        assert.isObject(el.inferred)
        assert.isObject(el.clipped)
        assert.isObject(el.combined)
        assert.isObject(el.centroid)
        assert.isObject(el.crisp)
        return undefined
      }
    }
  })).export(module)
