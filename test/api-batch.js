/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// api-batch.js
// Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved

const vows = require('perjury')
const { assert } = vows

const _ = require('lodash')

const env = require('./env')

const base = {
  'When we start the EvaluatorServer': {
    topic () {
      try {
        const EvaluatorServer = require('../lib/evaluator-server')
        const server = new EvaluatorServer(env)
        server.start(err => {
          if (err) {
            return this.callback(err)
          } else {
            return this.callback(null, server)
          }
        })
      } catch (error) {
        const err = error
        this.callback(err)
      }
      return undefined
    },
    'it works' (err, server) {
      assert.ifError(err)
      return assert.isObject(server)
    },
    'teardown' (server) {
      const { callback } = this
      server.stop(err => {
        if (err) {
          console.error(err)
        }
        callback(null)
      })
      return undefined
    }
  }
}

const apiBatch = function (rest) {
  const batch = _.cloneDeep(base)
  _.assign(batch['When we start the EvaluatorServer'], rest)
  return batch
}

module.exports = apiBatch
