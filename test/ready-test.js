// ready-test.js
// Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const apiBatch = require('./api-batch')
const lr = require('./lr')

lr()

vows
  .describe('/ready endpoint')
  .addBatch(apiBatch({
    'and we check readiness': {
      topic () {
        request.get('http://localhost:2342/ready', this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
        assert.equal(response.statusCode, 200)
        return assert.equal(JSON.parse(body).status, 'OK')
      }
    }
  })).export(module)
