// evaluate-scale-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')
const async = require('async')
const debug = require('debug')('evaluator:evaluate-scale-test')

const apiBatch = require('./api-batch')
const lr = require('./lr')

lr()

const eq = (a, b) => a === b

const defaultSetNames = function (count) {
  switch (count) {
    case 1:
      return ['medium']
    case 2:
      return ['low', 'high']
    case 3:
      return ['low', 'medium', 'high']
    case 4:
      return ['very low', 'low', 'high', 'very high']
    case 5:
      return ['very low', 'low', 'medium', 'high', 'very high']
    case 6:
      return ['very low', 'low', 'medium low', 'medium high', 'high', 'very high']
    case 7:
      return ['very low', 'low', 'medium low', 'medium',
        'medium high', 'high', 'very high']
    case 8:
      return ['very very low', 'very low', 'low', 'medium low',
        'medium high', 'high', 'very high', 'very very high']
    case 9:
      return ['very very low', 'very low', 'low', 'medium low', 'medium',
        'medium high', 'high', 'very high', 'very very high']
    default:
      return _.map(__range__(1, count - 1, true), i => `set ${i}`)
  }
}

const makeSets = function (count, low, high) {
  let sets
  if (eq(low, high)) {
    // We create a singular value, but account for the
    // possibility of lower or higher values

    // Epsilon used in fuzzy-controller is 1e-8, so we try to beat that
    // by 2 OOM

    let epsilon
    if (eq(low, 0.0)) {
      epsilon = 1e-6
    } else {
      epsilon = low * 1e-6
    }

    sets = {
      low: [low - epsilon, low],
      medium: [low - epsilon, low, low + epsilon],
      high: [low, low + epsilon]
    }
  } else if (eq(low, 0) && eq(high, 1)) {
    sets = {
      false: [-1, 0, 1],
      true: [0, 1, 2]
    }
  } else {
    const names = defaultSetNames(count)

    const interval = (high - low) / (count - 1)

    sets = {}

    for (let i = 0, end = count - 1, asc = end >= 0; asc ? i <= end : i >= end; asc ? i++ : i--) {
      if (i === 0) { // left shoulder
        sets[names[i]] = [low, low + interval]
      } else if (i === (count - 1)) { // left shoulder
        sets[names[i]] = [high - interval, high]
      } else {
        sets[names[i]] = [
          low + ((i - 1) * interval),
          low + (i * interval),
          low + ((i + 1) * interval)
        ]
      }
    }
  }

  return sets
}

const makeAgent = function (nin, nout, ns, low, high) {
  let name, o
  let asc, end
  if (ns == null) { ns = 5 }
  if (low == null) { low = 0 }
  if (high == null) { high = 100 }
  const agent = {
    inputs: {},
    outputs: {},
    rules: []
  }

  for (o = 0, end = nout, asc = end >= 0; asc ? o <= end : o >= end; asc ? o++ : o--) {
    name = `output${o}`
    agent.outputs[name] = makeSets(ns, low, high)
  }

  for (let i = 0, end1 = nin, asc1 = end1 >= 0; asc1 ? i <= end1 : i >= end1; asc1 ? i++ : i--) {
    let asc2
    let end2
    name = `input${i}`
    agent.inputs[name] = makeSets(ns, low, high)
    for (o = 0, end2 = nout, asc2 = end2 >= 0; asc2 ? o <= end2 : o >= end2; asc2 ? o++ : o--) {
      const oname = `output${o}`
      agent.rules.push(`${name} INCREASES ${oname}`)
    }
  }

  return agent
}

const randomInputs = function (nin, low, high) {
  const inputs = {}
  for (let i = 0, end = nin - 1, asc = end >= 0; asc ? i <= end : i >= end; asc ? i++ : i--) {
    const name = `input${i}`
    inputs[name] = _.random(low, high)
  }
  return inputs
}

const scaleBatch = function (nin, nout, nsets, ncalls, nconc, rest) {
  if (rest == null) { rest = {} }
  const low = 0
  const high = 100

  const agent = makeAgent(nin, nout, nsets)

  debug(JSON.stringify(agent))

  const batch = {
    topic () {
      const evaluate = function (i, callback) {
        const url = 'http://localhost:2342/evaluate'
        const properties = agent
        const inputs = randomInputs(nin, low, high)
        debug(JSON.stringify(inputs))
        const options = {
          url,
          json: {
            properties,
            inputs
          }
        }
        return request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
      }
      async.timesLimit(ncalls, nconc, evaluate, this.callback)
      return undefined
    },
    'it works' (err, els) {
      assert.ifError(err)
      assert.isArray(els)
      assert.lengthOf(els, ncalls)
      return (() => {
        const result = []
        for (let j = 0; j < els.length; j++) {
          const el = els[j]
          assert.isObject(el)
          assert.isObject(el.fuzzified)
          assert.isArray(el.rules)
          for (let i = 0; i < el.rules.length; i++) {
            const rule = el.rules[i]
            assert.isNumber(rule, `Rule ${i} of el ${j} is not a number: ${rule}`)
          }
          assert.isObject(el.inferred)
          assert.isObject(el.clipped)
          assert.isObject(el.combined)
          assert.isObject(el.centroid)
          result.push(assert.isObject(el.crisp))
        }
        return result
      })()
    }
  }

  _.assign(batch, rest)

  return batch
}

vows
  .describe('Evaluate scaling')
  .addBatch(apiBatch({
    'and we send a lot of evaluations to a 1I1O agent':
      scaleBatch(1, 1, 5, 128, 16, {
        'and we send a lot of evaluations to a 16I1O agent':
        scaleBatch(16, 1, 5, 128, 16, {
          'and we send a lot of evaluations to a 8I4O agent':
          scaleBatch(8, 4, 5, 128, 16, {
            'and we send a lot of evaluations to a 8I4O9S agent':
            scaleBatch(8, 4, 9, 128, 16)
          }
          )
        }
        )
      }
      )
  })).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}
