Configuration options
---------------------

The evaluator server is a subclass of [https://github.com/fuzzy-ai/microservice](Microservice), so
it uses all the same environment variables for basic configuration.

Endpoints
---------

evaluate
========

This endpoint takes a JSON object as a payload with two properties:

* properties: properties of a fuzzy-controller or fuzzy agent. inputs, outputs,
  rules. Probably doesn't work with parsed_rules.
* inputs: inputs for the agent.

The server instantiates the agent, and then passes it the inputs to evaluate. It
returns a JSON object with the following properties:

* fuzzified: fuzzified version of the inputs
* rules: rules that fired, indexed
* inferred: inferred fuzzy values of outputs
* clipped: clipped polygons for outputs
* combined: combined polygons for outputs
* centroid: centroids for polygons for outputs
* crisp: output values

The server does no DB access; just CPU-intensive stuff.

live
====

Returns `{status: "OK"}`. Good for checking if the service is live.

ready
=====

Returns `{status: "OK"}`. Good for checking if the service is ready.
